#!/bin/sh

echo "Create Keystore for instance :"
echo $INSTANCE_NAME

# création d'un keystore
echo "Create logstash Keystore"
echo "y" | logstash-keystore create

echo "jdbc connection string is : mysql://$JDBC_HOST:$JDBC_PORT/$JDBC_DATABASE"

echo "Fill keys with env data"
echo $INSTANCE_NAME | logstash-keystore add INSTANCE_NAME
echo $JDBC_HOST | logstash-keystore add JDBC_HOST
echo $JDBC_DATABASE | logstash-keystore add JDBC_DATABASE
echo $JDBC_PORT | logstash-keystore add JDBC_PORT
echo $JDBC_PWD | logstash-keystore add JDBC_PWD
echo $JDBC_USER | logstash-keystore add JDBC_USER
echo $ELASTIC_PASSWORD | logstash-keystore add ELASTIC_PASSWORD

