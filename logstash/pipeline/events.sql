SELECT 
event.id AS 'event_id', 
CONCAT(CAST(event.user_id as char), '-event') AS 'user_id_to_hash',
DATE_FORMAT(event.created_date,'%Y-%m-%d') as 'created_date_s',

COALESCE(event.name,'') as 'event_name',

CASE 
    WHEN (event.status=2 OR COALESCE(event.to_date, event.from_date)<NOW()) THEN 'Inactif' 
    WHEN (event.status=1) THEN 'Actif'
    ELSE 'Non déterminé'
END AS 'status', 

address.latitude AS 'latitude', address.longitude AS 'longitude',
address.region AS 'dept',
address.address_locality AS 'city',
address.postal_code AS 'postal_code',
address.macro_region AS 'region',
(select GROUP_CONCAT(territory.name) from territory, address_territory where territory.id=address_territory.territory_id AND address_territory.address_id=address.id) territory,

criteria.driver AS 'as_driver',
criteria.passenger AS 'as_passenger',
criteria.solidary_exclusive AS 'solidary_excl',

CASE 
    WHEN (criteria.frequency=1) THEN 'Ponctuel' 
    WHEN (criteria.frequency=2) THEN 'Régulier' 
    ELSE 'Non déterminé' 
END AS 'frequency',

DATE_FORMAT(event.from_date,'%Y-%m-%d') as 'from_date_s',
DATE_FORMAT(COALESCE(event.to_date, event.from_date),'%Y-%m-%d') AS 'to_date_s',

(SELECT COUNT(DISTINCT(ask.id))
    FROM ask, matching 
    WHERE   (matching.proposal_offer_id=proposal.id OR matching.proposal_request_id=proposal.id) 
            AND ask.matching_id=matching.id
            AND ask.status IN (4, 5)
) nb_ask_accepted,

(SELECT COUNT(DISTINCT(ask.id)) 
    FROM ask, matching 
    WHERE   (matching.proposal_offer_id=proposal.id OR matching.proposal_request_id=proposal.id) 
            AND ask.matching_id=matching.id
            AND ask.status IN (6, 7)
) nb_ask_declined,

(SELECT COUNT(DISTINCT(ask.id)) 
    FROM ask, matching 
    WHERE   (matching.proposal_offer_id=proposal.id OR matching.proposal_request_id=proposal.id) 
            AND ask.matching_id=matching.id
) nb_ask,

CASE 
    WHEN criteria.frequency=1 THEN 1
    WHEN criteria.frequency=2 THEN ROUND(GREATEST(DATEDIFF(criteria.to_date,criteria.from_date)+1,7) * ( criteria.mon_check + criteria.tue_check + criteria.wed_check + criteria.thu_check + criteria.fri_check + criteria.sat_check + criteria.sun_check ) / 7)
    ELSE 0
END AS 'nb_travel'

FROM event
LEFT JOIN proposal ON event.id=proposal.event_id
LEFT JOIN criteria ON proposal.criteria_id=criteria.id 
LEFT JOIN address ON event.address_id=address.id
;
