SELECT 
ask.id AS 'ask_id', 
CONCAT(CAST(ask.user_id as char), '-ask') AS 'user_id_to_hash',
DATE_FORMAT(ask.created_date,'%Y-%m-%d') as 'created_date_s',
-- const STATUS_INITIATED = 1; const STATUS_PENDING_AS_DRIVER = 2; const STATUS_PENDING_AS_PASSENGER = 3; const STATUS_ACCEPTED_AS_DRIVER = 4; 
-- const STATUS_ACCEPTED_AS_PASSENGER = 5; const STATUS_DECLINED_AS_DRIVER = 6; const STATUS_DECLINED_AS_PASSENGER = 7;
CASE 
    WHEN (ask.status=1) THEN 'Prise de contact'
    WHEN (ask.status=2) THEN 'Attente de réponse du conducteur'  
    WHEN (ask.status=3) THEN 'Attente de réponse du passager'
    WHEN (ask.status=4) THEN 'Accord du conducteur'
    WHEN (ask.status=5) THEN 'Accord du passager'
    WHEN (ask.status=6) THEN 'Refus du conducteur'
    WHEN (ask.status=7) THEN 'Refus du passager'
    ELSE 'Non déterminé' 
END AS 'detailed_status',
CASE
    WHEN (ask.status IN (1, 2, 3 ) AND COALESCE(criteria.to_date, criteria.from_date)<now()) THEN 'Expiré'      
    WHEN (ask.status IN (1, 2, 3 ) AND COALESCE(criteria.to_date, criteria.from_date)>=now()) THEN 'En attente'
    WHEN (ask.status IN (4, 5) ) THEN 'Accord'
    WHEN (ask.status IN (6, 7) ) THEN 'Refus'
    ELSE 'Non déterminé' 
END AS 'status',

COALESCE(evenement.name,'') as 'event_name',
COALESCE(community.name,'') as 'community_name',
COALESCE(proposal.dynamic, 0) AS 'is_dynamic',

adresse_origine.latitude AS 'origin_latitude', adresse_origine.longitude AS 'origin_longitude',
adresse_origine.region AS 'origin_dept',
adresse_origine.address_locality AS 'origin_city',
adresse_origine.postal_code AS 'origin_postal_code',
adresse_origine.macro_region AS 'origin_region',
(select GROUP_CONCAT(territory.name) from territory, address_territory where territory.id=address_territory.territory_id AND address_territory.address_id=adresse_origine.id ) territory_origine,

adresse_destination.latitude AS 'destination_latitude', adresse_destination.longitude AS 'destination_longitude',
adresse_destination.region AS 'destination_dept',
adresse_destination.address_locality AS 'destination_city',
adresse_destination.postal_code AS 'destination_postal_code',
adresse_destination.macro_region AS 'destination_region',
(select GROUP_CONCAT(territory.name) from territory, address_territory where territory.id=address_territory.territory_id AND address_territory.address_id=adresse_destination.id ) territory_destination,

criteria.seats_passenger AS 'seats_passenger',
criteria.driver_price AS 'driver_price',
criteria.passenger_price AS 'passenger_price',
criteria.solidary AS 'solidary',
criteria.solidary_exclusive AS 'solidary_exclusive',

CASE 
    WHEN (criteria.frequency=1) THEN 'Ponctuel' 
    WHEN (criteria.frequency=2) THEN 'Régulier' 
    ELSE 'Non déterminé' 
END AS 'frequency', 

DATE_FORMAT(criteria.from_date,'%Y-%m-%d') as 'from_date_s',
DATE_FORMAT(COALESCE(criteria.to_date, criteria.from_date),'%Y-%m-%d') AS 'to_date_s',

ROUND(matching.common_distance / 1000) as 'common_distance_km',

ROUND(matching.original_distance / 1000) as 'original_distance_km',
ROUND(matching.new_distance / 1000) as 'new_distance_km',
ROUND(matching.detour_distance / 1000) as 'detour_distance_km',
matching.detour_distance_percent as 'detour_distance_percent',

ROUND(matching.original_duration / 60) as 'original_duration_min',
ROUND(matching.new_duration / 60) as 'new_duration',
ROUND(matching.detour_duration / 60) as 'detour_duration',
matching.detour_duration_percent as 'detour_duration_percent',

CASE 
    WHEN criteria.frequency=1 THEN 1
    WHEN criteria.frequency=2 THEN ROUND(GREATEST(DATEDIFF(criteria.to_date,criteria.from_date)+1,7) * ( criteria.mon_check + criteria.tue_check + criteria.wed_check + criteria.thu_check + criteria.fri_check + criteria.sat_check + criteria.sun_check ) / 7)
    ELSE 0
END AS 'nb_travel',

CASE 
    WHEN criteria.frequency=1 THEN ROUND(matching.common_distance / 1000)
    WHEN criteria.frequency=2 THEN ROUND(matching.common_distance * ROUND(GREATEST(DATEDIFF(criteria.to_date,criteria.from_date)+1,7) * ( criteria.mon_check + criteria.tue_check + criteria.wed_check + criteria.thu_check + criteria.fri_check + criteria.sat_check + criteria.sun_check ) / 7) / 1000)
    ELSE 0
END AS 'cumulated_common_distance'

FROM ask
LEFT JOIN criteria ON ask.criteria_id=criteria.id
LEFT JOIN matching ON ask.matching_id=matching.id 
LEFT JOIN waypoint waypoint_origine ON waypoint_origine.ask_id=ask.id AND waypoint_origine.position=0 AND waypoint_origine.ask_id is not NULL
LEFT JOIN waypoint waypoint_destination ON waypoint_destination.ask_id=ask.id AND waypoint_destination.destination=1 AND waypoint_destination.ask_id is not NULL
LEFT JOIN address adresse_origine ON waypoint_origine.address_id=adresse_origine.id
LEFT JOIN address adresse_destination ON waypoint_destination.address_id=adresse_destination.id
LEFT JOIN proposal ON proposal.id=matching.proposal_offer_id
LEFT JOIN proposal_community ON proposal_community.proposal_id=proposal.id
LEFT JOIN community ON proposal_community.community_id=community.id 
LEFT JOIN event evenement ON proposal.event_id=evenement.id

WHERE criteria.frequency=1 OR ask.status IN (1, 2, 3) OR (ask.status >3 AND criteria.frequency=2 AND ( criteria.mon_check + criteria.tue_check + criteria.wed_check + criteria.thu_check + criteria.fri_check + criteria.sat_check + criteria.sun_check )>0)
;
