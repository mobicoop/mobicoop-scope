SELECT carpool_item.id as 'carpool_id',

DATE_FORMAT(carpool_item.item_date,'%Y-%m-%d') as 'carpool_date_s',
DATE_FORMAT(carpool_item.created_date,'%Y-%m-%d') as 'created_date_s',

CASE 
    WHEN (carpool_item.type=1) THEN 'Aller simple'
    WHEN (carpool_item.type=2) THEN 'Aller'  
    WHEN (carpool_item.type=3) THEN 'Retour' 
    ELSE 'Indéterminé' 
END AS 'type',

CASE 
    WHEN (carpool_item.item_status=0) THEN 'Non confirmé'
    WHEN (carpool_item.item_status=1) THEN 'Confirmé'  
    WHEN (carpool_item.item_status=2) THEN 'Non covoituré' 
    ELSE 'Indéterminé' 
END AS 'status',

CASE 
    WHEN (carpool_item.debtor_status=-1) THEN 'Paiement inactif'
    WHEN (carpool_item.debtor_status=0) THEN 'Non payé'  
    WHEN (carpool_item.debtor_status=1) THEN 'Electronique en attente' 
    WHEN (carpool_item.debtor_status=2) THEN 'Direct en attente'
    WHEN (carpool_item.debtor_status=3) THEN 'Electronique confirmé'
    WHEN (carpool_item.debtor_status=4) THEN 'Direct confirmé'
    ELSE 'Indéterminé' 
END AS 'debtor_status',

CASE 
    WHEN (carpool_item.creditor_status=-1) THEN 'Paiement inactif'
    WHEN (carpool_item.creditor_status=0) THEN 'Direct à confirmer'  
    WHEN (carpool_item.creditor_status=1) THEN 'Electronique en attente' 
    WHEN (carpool_item.creditor_status=3) THEN 'Electronique confirmé'
    WHEN (carpool_item.creditor_status=4) THEN 'Direct confirmé'
    ELSE 'Indéterminé' 
END AS 'creditor_status',

CASE 
    WHEN carpool_item.unpaid_date IS NOT NULL THEN true
    ELSE false 
END AS 'unpaid',

carpool_item.amount as 'amount'

FROM `carpool_item`
ORDER by carpool_item.id