SELECT 
proposal.id AS 'proposal_id', 
CONCAT(CAST(proposal.user_id as char), '-proposal') AS 'user_id_to_hash',
CASE
    WHEN (COALESCE(criteria.to_date, criteria.from_date)<NOW()) THEN 'Inactive' 
    ELSE 'Active'
END as 'status',
DATE_FORMAT(proposal.created_date,'%Y-%m-%d') as 'created_date_s',
CASE 
    WHEN (proposal.private=0) THEN 'Annonce' 
    ELSE 'Recherche' 
END AS 'search', 

COALESCE(proposal.dynamic, 0) AS 'is_dynamic',

CASE 
    WHEN (proposal.type=1) THEN 'Aller simple'
    WHEN (proposal.type=2) THEN 'Aller'  
    WHEN (proposal.type=3) THEN 'Retour' 
    ELSE 'Non déterminé' 
END AS 'type', 

COALESCE(evenement.name,'') as 'event_name',
COALESCE(community.name,'') as 'community_name',

adresse_origine.latitude AS 'origin_latitude', adresse_origine.longitude AS 'origin_longitude',
adresse_origine.region AS 'origin_dept',
adresse_origine.address_locality AS 'origin_city',
adresse_origine.postal_code AS 'origin_postal_code',
adresse_origine.macro_region AS 'origin_region',
(   select GROUP_CONCAT(territory.name) 
    from territory, address_territory 
    where territory.id=address_territory.territory_id AND address_territory.address_id=adresse_origine.id 
) territory_origine,

adresse_destination.latitude AS 'destination_latitude', adresse_destination.longitude AS 'destination_longitude',
adresse_destination.region AS 'destination_dept',
adresse_destination.address_locality AS 'destination_city',
adresse_destination.postal_code AS 'destination_postal_code',
adresse_destination.macro_region AS 'destination_region',
(   select GROUP_CONCAT(territory.name) 
    from territory, address_territory 
    where territory.id=address_territory.territory_id 
    AND address_territory.address_id=adresse_destination.id 
) territory_destination,

COALESCE(criteria.driver, 1) AS 'as_driver',
COALESCE(criteria.passenger, 1) AS 'as_passenger',
criteria.solidary_exclusive AS 'solidary_excl',

CASE 
    WHEN (criteria.frequency=1) THEN 'Ponctuel' 
    WHEN (criteria.frequency=2) THEN 'Régulier' 
    ELSE 'Non déterminé' 
END AS 'frequency',

DATE_FORMAT(criteria.from_date,'%Y-%m-%d') as 'from_date_s',
DATE_FORMAT(COALESCE(criteria.to_date, criteria.from_date),'%Y-%m-%d') AS 'to_date_s',

(count(distinct(aop.id))+count(distinct(arp.id))) as nb_ask_pending,
(count(distinct(aoa.id))+count(distinct(ara.id))) as nb_ask_accepted,
(count(distinct(aod.id))+count(distinct(ard.id))) as nb_ask_declined,
(count(distinct(aoi.user_id))+count(distinct(ari.user_id))) as nb_interacting_users,
(count(distinct(ao.id))+count(distinct(ar.id))) as nb_ask,

COALESCE(solidary.id, 0) as solidary_id,
(solidary.id IS NOT NULL) as is_solidary

FROM proposal
LEFT JOIN criteria ON proposal.criteria_id=criteria.id 
LEFT JOIN waypoint waypoint_origine ON waypoint_origine.proposal_id=proposal.id AND waypoint_origine.position=0 AND waypoint_origine.proposal_id is not NULL
LEFT JOIN waypoint waypoint_destination ON waypoint_destination.proposal_id=proposal.id AND waypoint_destination.destination=1 AND waypoint_destination.proposal_id is not NULL
LEFT JOIN address adresse_origine ON waypoint_origine.address_id=adresse_origine.id
LEFT JOIN address adresse_destination ON waypoint_destination.address_id=adresse_destination.id
LEFT JOIN event evenement ON proposal.event_id=evenement.id
LEFT JOIN proposal_community ON proposal_community.proposal_id=proposal.id
LEFT JOIN community ON proposal_community.community_id=community.id
LEFT JOIN solidary ON solidary.proposal_id=proposal.id
LEFT JOIN matching mo ON mo.proposal_offer_id=proposal.id
LEFT JOIN matching mr ON mr.proposal_request_id=proposal.id
LEFT JOIN ask ao ON mo.id = ao.matching_id
LEFT JOIN ask ar ON mr.id = ar.matching_id
LEFT JOIN ask aoi ON mo.id = aoi.matching_id AND aoi.user_id <> proposal.user_id
LEFT JOIN ask ari ON mr.id = ari.matching_id AND ari.user_id <> proposal.user_id
LEFT JOIN ask aop ON mo.id = aop.matching_id AND aop.status IN (2, 3)
LEFT JOIN ask arp ON mr.id = arp.matching_id AND arp.status IN (2, 3)
LEFT JOIN ask aoa ON mo.id = aoa.matching_id AND aoa.status IN (4, 5)
LEFT JOIN ask ara ON mr.id = ara.matching_id AND ara.status IN (4, 5)
LEFT JOIN ask aod ON mo.id = aod.matching_id AND aod.status IN (6, 7)
LEFT JOIN ask ard ON mr.id = ard.matching_id AND ard.status IN (6, 7)
WHERE proposal.id > :sql_last_value AND proposal.external IS NULL
GROUP BY proposal.id
ORDER BY proposal.id 
LIMIT 20000 
;