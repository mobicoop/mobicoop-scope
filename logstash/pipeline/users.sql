SELECT 
u.id AS 'user_id_to_hash', 
u.status AS 'user_status', 
DATE_FORMAT(u.last_activity_date,'%Y-%m-%d') AS 'last_activity_date_s',
u.last_activity_date > DATE_SUB(NOW(), INTERVAL 6 MONTH) as 'active_user',
TIMESTAMPDIFF(YEAR,u.birth_date, NOW()) as 'user_age',
u.email AS 'user_email', 
DATE_FORMAT(u.created_date,'%Y-%m-%d') as 'user_created_date_s',
DATE_FORMAT(u.validated_date,'%Y-%m-%d') as 'user_validated_date_s',
CASE 
    WHEN (u.validated_date IS NULL AND u.status=1) THEN 'Non validé' 
    WHEN (u.validated_date IS NOT NULL AND u.status=1) THEN 'Validé' 
    WHEN u.status=2 THEN 'Inactif' 
    WHEN u.status=3 THEN 'Désinscrit' 
    ELSE 'Undefined' 
END AS 'user_status_label', 
CASE 
    WHEN (u.gender=1) THEN 'Femme' 
    WHEN (u.gender=2) THEN 'Homme' 
    WHEN (u.gender=3) THEN 'Autre' 
    ELSE 'Undefined' 
END AS 'user_gender', 

domicile.latitude AS 'user_latitude', domicile.longitude AS 'user_longitude',
domicile.region AS 'user_dept',
domicile.address_locality AS 'user_city',
domicile.postal_code AS 'user_postal_code',
domicile.macro_region AS 'user_region',
(select GROUP_CONCAT(community.name) from community, community_user where community.id=community_user.community_id AND  community_user.user_id=u.id) community_name,
(select GROUP_CONCAT(territory.name) from territory, address_territory where territory.id=address_territory.territory_id AND address_territory.address_id=domicile.id)  territory_name_domicile,
(select GROUP_CONCAT(territory.name) from territory 
    where territory.id IN (
        select distinct(address_territory.territory_id) 
        FROM ask, waypoint, address_territory 
        WHERE waypoint.ask_id=ask.id 
        AND (waypoint.destination = 1 or waypoint.position = 0) 
        AND ask.user_id = u.id 
        AND ask.status IN (4, 5) 
        AND address_territory.address_id=waypoint.address_id 
    )
 ) territory_name_ask,
(select GROUP_CONCAT(territory.name) from territory 
    where territory.id IN (
    select distinct(address_territory.territory_id) FROM proposal, waypoint, address_territory WHERE waypoint.proposal_id=proposal.id AND waypoint.destination IN (0,1) AND proposal.user_id=u.id AND proposal.private=0 AND address_territory.address_id=waypoint.address_id
    )
 ) territory_name_proposal,
(SELECT COUNT(proposal.id) FROM proposal, criteria WHERE proposal.user_id = u.id AND proposal.criteria_id=criteria.id AND criteria.driver=1 AND proposal.private=0) nb_proposal_offer ,
(SELECT COUNT(proposal.id) FROM proposal, criteria WHERE proposal.user_id = u.id AND proposal.criteria_id=criteria.id AND criteria.driver<>1 AND proposal.private=0) nb_proposal_ask ,

(SELECT COUNT(proposal.id) FROM proposal WHERE proposal.user_id = u.id AND proposal.private=1) nb_search ,
(SELECT COUNT(proposal.id) FROM proposal WHERE proposal.user_id = u.id AND proposal.private=0) nb_offer ,
(SELECT COUNT(proposal.id) FROM proposal WHERE proposal.user_id = u.id ) nb_proposal ,

(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id AND ask.status=1) nb_ask_initiated,
(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id AND ask.status=2) nb_ask_pending_as_driver,
(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id AND ask.status=3) nb_ask_pending_as_passenger,
(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id AND ask.status IN (2, 3) ) nb_ask_pending,

(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id AND ask.status=4) nb_ask_accepted_as_driver,
(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id AND ask.status=5) nb_ask_accepted_as_passenger,
(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id AND ask.status IN (4, 5) ) nb_ask_accepted,

(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id AND ask.status=6 ) nb_ask_declined_as_driver,
(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id AND ask.status=7 ) nb_ask_declined_as_passenger,
(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id AND ask.status IN (6, 7) ) nb_ask_declined,

(SELECT COUNT(ask.id) FROM ask WHERE ask.user_id = u.id  ) nb_ask,

-- nb of reviews
(SELECT COUNT(review.id) FROM review WHERE review.reviewer_id = u.id  ) nb_review,

-- nb of online payments
(SELECT COUNT(carpool_item.id) FROM carpool_item WHERE carpool_item.debtor_user_id = u.id AND debtor_status=3 AND item_status=1) nb_pay_online,

-- sum of online payments
(SELECT SUM(carpool_item.amount) FROM carpool_item WHERE carpool_item.debtor_user_id = u.id AND debtor_status=3 AND item_status=1) amount_pay_online,

-- nb of direct payments
(SELECT COUNT(carpool_item.id) FROM carpool_item WHERE carpool_item.debtor_user_id = u.id AND debtor_status=4 AND item_status=1) nb_pay_direct,

-- sum of direct payments
(SELECT SUM(carpool_item.amount) FROM carpool_item WHERE carpool_item.debtor_user_id = u.id AND debtor_status=4 AND item_status=1) amount_pay_direct,

--  le créateur de la proposalOffer est conducteur, le passager est le créateur de la proposalRequest
(SELECT 
    SUM(CASE 
        WHEN criteria.frequency=1 THEN 1
        WHEN criteria.frequency=2 THEN ROUND((1+DATEDIFF(criteria.to_date,criteria.from_date)) * ( criteria.mon_check + criteria.tue_check + criteria.wed_check + criteria.thu_check + criteria.fri_check + criteria.sat_check + criteria.sun_check ) / 7)
        ELSE 0
    END 
    ) AS nb_travel
    FROM ask, criteria, matching, proposal 
    WHERE ask.criteria_id=criteria.id AND ask.matching_id = matching.id 
    AND ask.status IN (4, 5) AND ask.user_id=u.id 
    AND matching.proposal_offer_id=proposal.id AND proposal.user_id=u.id
) nb_travel_as_driver,

(SELECT 
    SUM(CASE 
        WHEN criteria.frequency=1 THEN 1
        WHEN criteria.frequency=2 THEN ROUND((1+DATEDIFF(criteria.to_date,criteria.from_date)) * ( criteria.mon_check + criteria.tue_check + criteria.wed_check + criteria.thu_check + criteria.fri_check + criteria.sat_check + criteria.sun_check ) / 7)
        ELSE 0
    END 
    ) AS nb_travel
    FROM ask, criteria, matching, proposal 
    WHERE ask.criteria_id=criteria.id AND ask.matching_id = matching.id 
    AND ask.status IN (4, 5) AND ask.user_id=u.id 
    AND matching.proposal_request_id=proposal.id AND proposal.user_id=u.id
) nb_travel_as_passenger,

(SELECT 
    SUM(CASE 
        WHEN criteria.frequency=1 THEN 1
        WHEN criteria.frequency=2 THEN ROUND((1+DATEDIFF(criteria.to_date,criteria.from_date)) * ( criteria.mon_check + criteria.tue_check + criteria.wed_check + criteria.thu_check + criteria.fri_check + criteria.sat_check + criteria.sun_check ) / 7)
        ELSE 0
    END 
    ) AS nb_travel
    FROM ask, criteria 
    WHERE ask.criteria_id=criteria.id AND ask.status IN (4,5) AND ask.user_id=u.id ) nb_travel,

solidary_user.beneficiary as is_beneficiary,
solidary_user.volunteer as is_volunteer,
(SELECT COUNT(solidary.id) FROM proposal, solidary WHERE proposal.user_id = u.id AND solidary.proposal_id=proposal.id ) nb_solidary_proposal 

FROM user u
LEFT JOIN address domicile ON u.id=domicile.user_id AND domicile.home=1
LEFT JOIN solidary_user ON solidary_user.id=u.solidary_user_id 
WHERE u.id > :sql_last_value 
ORDER by u.id 
LIMIT 10000 
;

