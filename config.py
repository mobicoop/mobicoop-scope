import os
import json
import subprocess
from shutil import copyfile
import datetime
import getopt
import sys
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

mail_content = ""

def send_mail () :
    global mail_content
    if mail_content != "" :
        print("Sending email to :", os.environ['EMAIL_TO'])
        smpt_server = smtplib.SMTP(host=os.environ['SMPT_HOST'], port=os.environ['SMPT_PORT'])
        smpt_server.set_debuglevel(1)
        smpt_server.starttls()
        smpt_server.login(os.environ['SMPT_LOGIN'], os.environ['SMPT_PASSWORD'])
        msg = MIMEMultipart()
        msg['From']=os.environ['EMAIL_FROM']
        msg['To']=os.environ['EMAIL_TO']
        msg['Subject']="Scope report"
        msg.attach(MIMEText(mail_content, 'plain'))
        smpt_server.send_message(msg)
        del msg
        smpt_server.quit()

def add_log (message) :
    global mail_content
    now = datetime.datetime.now()
    mail_content += now.strftime("%Y-%m-%d %H:%M:%S")
    mail_content += " - "
    mail_content += message
    mail_content += '\n'

def read_env_file() :
    with open('.env', 'r') as fh:
        vars_dict = dict(
            tuple(line.rstrip().split('='))
            for line in fh.readlines()
            if not line.startswith('#')
        )
    os.environ.update(vars_dict)

def read_sequence_id (instance_name, cube) :
    logstash_jdbc_last_run = 'logstash/config/.logstash_jdbc_last_run_' + cube + '_' + instance_name
    now = datetime.datetime.now()
    print(now.strftime("%Y-%m-%d %H:%M:%S")," - Reading index from :", logstash_jdbc_last_run)
    try: 
        with open(logstash_jdbc_last_run, 'r') as fh:
            line = fh.readlines()
            # looks like --- 30
            index = int(line[0][4:])
            print("Index :", index)
            return index
    except :
        print("Error reading index :", logstash_jdbc_last_run)
        return 0

def remove_sequence_file (instance_name, cube):
    now = datetime.datetime.now()
    print(now.strftime("%Y-%m-%d %H:%M:%S")," - Remove sequence file for :", instance_name)
    process = subprocess.Popen(['rm', 'logstash/config/.logstash_jdbc_last_run_' + cube + '_' + instance_name],
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    print(stdout, stderr)

def create_keystore() :
    process = subprocess.Popen(['docker-compose', 'run', '--rm', 'logstash', 'logstash-keystore', 'create'],
                        stdin =subprocess.PIPE,
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE,
                        universal_newlines=True,
                        bufsize=0)
    process.stdin.write("y\n")
    stdout, stderr = process.communicate()
    print(stdout, stderr)
    
def add_key_to_keystore (key, value):
    # Add a key to logstash.keystore
    print("Adding key :", key)
    process = subprocess.Popen(['docker-compose', 'run', '--rm', 'logstash', 'logstash-keystore', 'add', key],
                        stdin =subprocess.PIPE,
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE,
                        universal_newlines=True,
                        bufsize=0)
    process.stdin.write(value+"\n")
    stdout, stderr = process.communicate()
    print(stdout, stderr)

def create_indices(instance_name, elastic_pwd, cube, version=""):
    mapping_file    = 'elasticsearch/mappings/'+cube+'.json'
    index_name      = 'localhost:9200/mobicoop_'+cube+'_' + instance_name + version
    print('\n')
    print("Creating index:", index_name)
    print("with mapping_file:", mapping_file)
    print("It may fail if the index already exists.")
    process = subprocess.Popen(['curl', '--user', 'elastic:'+elastic_pwd, '-X PUT', index_name, "-H",  'Content-Type:application/json', '-d', '@' + mapping_file],
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    print(stdout)

def reindex(instance_name, elastic_pwd, cube, version_from, version_to):
    if (version_from == "" and version_to==""):
        print("cannot reindex without a specified version")
        return
    source_index_name   = 'mobicoop_'+cube+'_' + instance_name + version_from
    dest_index_name   = 'mobicoop_'+cube+'_' + instance_name + version_to

    print("Reindexing:"+ source_index_name)
    print("to:" + dest_index_name)
    data = "{\"source\": {\"index\":\"" + source_index_name + "\"},\"dest\":{\"index\":\"" + dest_index_name + "\"} }"

    process = subprocess.Popen(['curl', '--user', 'elastic:'+elastic_pwd, '-X POST', 'localhost:9200/_reindex', "-H",  'Content-Type:application/json', '-d', data],
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    print(stdout)
    print(stderr)

def create_role(instance_name, elastic_pwd, username, pwd, cubes):
    role_name   = 'mobicoop-admin-dashboard-'+instance_name
    api         = 'localhost:9200/_security/role/'+role_name
    formated_cubes = ','.join( [ '"mobicoop_'+cube+'_'+instance_name+'*"' for cube in cubes] )
    role = '{ "indices" : [ { "names" : [' + formated_cubes + '],"privileges" : ["read"],"allow_restricted_indices" : false}],'
    role += '"applications" : [{"application" : "kibana-.kibana","privileges" : ["feature_dashboard.read"],"resources" : ["*"]}],"run_as" : [ ],"metadata" : { },"transient_metadata" : {"enabled" : true}}'
    print("Adding role :", role_name)
    process = subprocess.Popen(['curl', '--user', 'elastic:'+elastic_pwd, '-X PUT', api, "-H",  'Content-Type:application/json', '-d', role],
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    print(stdout, stderr)

    api     = 'localhost:9200/_security/user/'+username
    user    = '{"password" : "'+ pwd +'", "roles" : [ "kibana_dashboard_only_user", "'+role_name+'" ]}'
    print("Adding role :", username)
    process = subprocess.Popen(['curl', '--user', 'elastic:'+elastic_pwd, '-X PUT', api, "-H",  'Content-Type:application/json', '-d', user],
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    print(stdout, stderr)

def run_logstash(logstash_filename):
    now = datetime.datetime.now()
    print(now.strftime("%Y-%m-%d %H:%M:%S")," - Running logstash -f "+logstash_filename)
    process = subprocess.Popen(['docker-compose', 'run', '--rm', 'logstash', 'logstash', '-f',  'pipeline/' +logstash_filename],
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    #print(stdout, stderr)

def run_logstash_while_index_is_increasing (logstash_filename, instance_name, cube, limit=50):
    now = datetime.datetime.now()
    print(now.strftime("%Y-%m-%d %H:%M:%S")," - Run Logstash While Index Is Increasing")
    nb_iterations = 0
    remove_sequence_file(instance_name, cube)
    old_index = 0
    run_logstash(logstash_filename)
    while (read_sequence_id (instance_name, cube) > old_index and nb_iterations<limit):
        old_index = read_sequence_id (instance_name, cube)
        run_logstash(logstash_filename)
        nb_iterations+=1
    if (nb_iterations == limit):
        print("Error. Iteration limit reached :", limit)
        add_log("Error. Iteration limit reached.")

def check_running_import():
    process = subprocess.Popen(['docker', 'ps'],
                        stdout=subprocess.PIPE, 
                        stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    if str(stdout).find("mobicoop-scope_logstash")>-1:
        return True
    return False

def import_to_scope(instance_name):
    
    now = datetime.datetime.now()
    print (now.strftime("%Y-%m-%d %H:%M:%S"))
    if check_running_import():
        print("Another import is already running. Won't start another one. Aborting...")
        add_log("Another import is already running. Won't start another one. Aborting...")
        return

    print("Loading data from platform to scope :", instance_name)
    
    print("Running tasks in batch mode...")
    print("In .env file, iteration limit is set to ", int(os.environ['NB_MAX_ITERATIONS']))
    run_logstash_while_index_is_increasing('users.config', instance_name, 'user', int(os.environ['NB_MAX_ITERATIONS']))
    run_logstash_while_index_is_increasing('proposals.config', instance_name, 'proposals', int(os.environ['NB_MAX_ITERATIONS']))

    print("Running tasks in single-shot mode...")
    run_logstash('update_active_status_proposal.config')
    run_logstash('update_inactive_status_proposal.config')
    run_logstash('asks.config')
    run_logstash('events.config')
    run_logstash('carpools.config')
    run_logstash('unsubscribe.config')
    run_logstash('deleted_proposals.config')
    run_logstash('deleted_asks.config')
    run_logstash('deleted_events.config')

def main():
    version_from="" 
    version_to=""
    options = sys.argv[1:]
    if len(options)>0:
        options_pairs = [ (options[2*i], options[2*i+1]) for i in range(0, int(len(options)/2))]
        for key, value in options_pairs:
            if key=="-from":
                version_from = value
            elif key=="-to":
                version_to= value
            else:
                print("This option is not allowed :"+key)
                print("usage : python config.py -from <version> -to <another_version>")
                return

    # Load JSON config file
    with open('config.json', 'r') as json_file:
        instances = json.load(json_file)
    # Load env data
    read_env_file()
    add_log("Starting a new import ")

    # for each instance
    for instance in instances:
        now = datetime.datetime.now()
        print(now.strftime("%Y-%m-%d %H:%M:%S")," -- Processing instance:", instance['name'])
        # creating indices if not already there
        print("-- Creating indices")
        create_indices( instance['name'], os.environ['ELASTIC_PASSWORD'], 'users', version_to)
        create_indices( instance['name'], os.environ['ELASTIC_PASSWORD'], 'asks', version_to)
        create_indices( instance['name'], os.environ['ELASTIC_PASSWORD'], 'proposals', version_to)
        create_indices( instance['name'], os.environ['ELASTIC_PASSWORD'], 'events', version_to)
        create_indices( instance['name'], os.environ['ELASTIC_PASSWORD'], 'carpools', version_to)
        # if a version is set, we are waiting for a re-index. No need to continue
        if version_from != "" or version_to !="" :
            print("-- A version is set. re-indexing the old data in new indexes post-fixed by "+ version_to)
            reindex( instance['name'], os.environ['ELASTIC_PASSWORD'], 'users', version_from, version_to)
            reindex( instance['name'], os.environ['ELASTIC_PASSWORD'], 'asks', version_from, version_to)
            reindex( instance['name'], os.environ['ELASTIC_PASSWORD'], 'proposals', version_from, version_to)
            reindex( instance['name'], os.environ['ELASTIC_PASSWORD'], 'events', version_from, version_to)
            reindex( instance['name'], os.environ['ELASTIC_PASSWORD'], 'carpools', version_from, version_to)
            print("Now check the new indices. Delete them if OK.")
        else:
            
            print("Creating Kibana role and user")
            create_role( instance['name'], os.environ['ELASTIC_PASSWORD'], instance['kibana_user'], instance['kibana_pwd'], ['users', 'solidary', 'asks', 'proposals', 'events', 'carpools'])
            
            # check if a keystore already exists
            if not os.path.isfile(os.path.join(os.getcwd(),'logstash/config/logstash.keystore.'+instance['name'])):
                print("Key store not found. Creating :", 'logstash/config/logstash.keystore.'+instance['name'])
                # create keystore
                create_keystore()
                # Add keys
                add_key_to_keystore('INSTANCE_NAME', instance['name'])
                add_key_to_keystore('JDBC_HOST', instance['jdbc_host'])
                add_key_to_keystore('JDBC_DATABASE', instance['jdbc_database_name'])
                add_key_to_keystore('JDBC_PORT', str(instance['jdbc_port']))
                add_key_to_keystore('JDBC_PWD', instance['jdbc_pwd'])
                add_key_to_keystore('JDBC_USER', instance['jdbc_user'])
                add_key_to_keystore('ELASTIC_PASSWORD', os.environ['ELASTIC_PASSWORD'])
                if os.environ['TEST_USER_EMAIL_PATTERN']:
                    add_key_to_keystore('TEST_USER_EMAIL_PATTERN', os.environ['TEST_USER_EMAIL_PATTERN'])
                
                # save it
                copyfile('logstash/config/logstash.keystore', 'logstash/config/logstash.keystore.'+instance['name'])
            else:
                # copy it to logstash.keystore
                copyfile('logstash/config/logstash.keystore.'+instance['name'], 'logstash/config/logstash.keystore')

            print("Launching import to scope")
            import_to_scope(instance['name'])
    add_log("End.")
    send_mail()

if __name__ == "__main__":
    main()
    
    
