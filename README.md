# Mobicoop Scope

Mobicoop Scope is the indicator dashboard for Mobicoop Platform.
The following shows the necessary steps in creating a new instance of Mobicoop Scope on a local machine.

# How to install

## Step 1 : Create a .env file

Copy the `.env.default` file to your `.env` configuration file at the root of the project.

2 required parameters should be set in `.env` file :

- `ELASTIC_PASSWORD` is the password for the superuser `elastic`. This credentials is used in ElasticSearch (for indexing new documents) and in Kibana.
- `LOGSTASH_KEYSTORE_PASS` is a temporary password used by Logstash to store the database credentials.
- `NB_MAX_ITERATIONS` is related to the import batch. For large databases, the import function splits the data import in many batches that run sequencially. A low number could lead to a partial import of the data.
- `ELASTIC_MEMORY_ALLOCATION` sets the minimun/maximum memroy allocation for the JVM heap size. Regarding Logstash, you can tune this parameter in the `/logstash/config/jvm.options` file.


## Step 2 : connect Scope to Mobicoop-Platform databases

Mobicoop Scope can store data from many Mobicoop-Platform databases. For each database, you have to configure :

- how to extract the data from the RDBMS through SQL queries (host, database name, login, password),
- a Kibana Role, to allow some Kibana users to see this data,
- a default Kibana user and the Mobicoop Platform API endpoint to embed a Kibana Dashboard in the Mobicoop Platform Back-office.

All theses settings should be in a `config.json` file. They should have the following format :

```
[
    {
        "name" : "anyname",
        "jdbc_host" : "xxx.xxx.xxx.xxx",
        "jdbc_database_name" : "database_name",
        "jdbc_port":3306,
        "jdbc_user": "database_user",
        "jdbc_pwd" : "database_pwd",
        "kibana_user" : "kibana_user_allowed_to_read_these_data",
        "kibana_pwd" : "kibana_pwd",
        "mobicoop_api": "https://api.yourmobicoop.com"
    },
    {
        "name" : "anyname",
        "jdbc_host" : "xxx.xxx.xxx.xxx",
        "jdbc_database_name" : "database_name",
        "jdbc_port":3306,
        "jdbc_user": "database_user",
        "jdbc_pwd" : "database_pwd",
        "kibana_user" : "kibana_user_allowed_to_read_these_data",
        "kibana_pwd" : "kibana_pwd",
        "mobicoop_api": "https://api.yourmobicoop.com"
    }
]
```

This JSON file contains an array of objects, corresponding to different databases, because a **Mobicoop Scope instance can host several Mobicoop Platform databases**.

For each Mobicoop Platform database, you have to set :

- a `name` : an "URL-compliant" name, whithout space or any symbol (/ , . " < > etc..). This name will be used to build the index names and the Kibana role name.
- the JDBC credentials, to get the data from the Mobicoop Platform database : `jdbc_host`, `jdbc_database_name`, `jdbc_port`, `jdbc_user` and `jdbc_pwd`,
- a default Kibana user : `kibana_user` and `kibana_pwd`. We suggest you use a strong password (typically containing letters including capitals, numbers and special characters such as &, #, etc.),
- the Mobicoop API endpoint : `mobicoop_api`. Scope will use this API to check the permission of a Mobicoop Platform user. A user needs to have an "administration" role to connect to Kibana. Ex: if `mobicoop_api` is set to `https://api.platform.com`, then the Scope Auth module will send a HTTP GET request to `https://api.platform.com/permission`. For a explanation of the authentication flow, have a look at [this](./docs/Scope_dashboard_flow-2.png).

## Step 3 : Build the images

### Install Docker and join the docker group.

Mobicoop Scope uses Docker containers.

Check that `docker` and `docker-compose` are already installed :

```
docker -v
```

If `docker` is not found, install it according to [this procedure](https://docs.docker.com/install/linux/docker-ce/ubuntu/).

Don't forget the [post-install procedure](https://docs.docker.com/install/linux/linux-postinstall/) if you do not want to preface the docker command with `sudo`.

If necessary, add your user to the docker group :

```
sudo usermod -aG docker $USER
newgrp docker
```

You should be able to run docker with :

```
docker run hello-world
```

### Install Docker compose

After docker, you need docker-compose. Check whether `docker-compose` is installed:

```
docker-compose -v
```

If `docker-compose` is not found, install it according to [this procedure](https://docs.docker.com/compose/install/).

### Copy SSL certs to enable an HTTPS access to Kibana

To enable an https access, you have to configure 2 files :

- `docker-compose.yml`
- `./nginx/config/nginx.conf`

If you **don't need an https access** for now, just copy the default versions of theses 2 files :

```
cp docker-compose.yml.default docker-compose.yml
cp ./nginx/config/nginx.conf.default ./nginx/config/nginx.conf
```

If you do, you have to provide the cert files :

Copy your cert files in `kibana/cert`. There should be two files :

- `fullchain.pem` and
- `privkey.pem`

You can generate theses files with [Let's Encrypt](https://letsencrypt.org)

Copy the `docker-compose.yml.default` file to a new `docker-compose.yml` and edit it :

- Replace `./kibana/config/kibana_without_https.yml` by `./kibana/config/kibana_with_https.yml`

- Un-comment the 4 lines related to the settings of the kibana container :

```
      - type: bind
        source: /the/path/to/kibana/cert <-- write the right path to the cert files you've copied
        target: /usr/share/kibana/cert
        read_only: true
```

- Un-comment the 4 lines related to the settings of the nginx container :

```
      - type: bind
        source: /etc/letsencrypt <-- usually OK if you used Let's encrypt
        target: /etc/letsencrypt
        read_only: true
```

Copy the `./nginx/config/nginx.conf.default` file to a new `./nginx/config/nginx.conf` and edit it :

- Un-comment and update the lines related to the SSL certificates :

```
    listen 443 ssl;
    server_name YOUR_SERVER_NAME;
    server_tokens off;
    ssl_certificate /etc/letsencrypt/live/YOUR_SERVER_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/YOUR_SERVER_NAME/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
```

- and configure the proxy tu use HTTPS to deal with Kibana :

  Replace `proxy_pass http://kibana-app;` by `proxy_pass https://kibana-app;` (mind the "s").

The Scope Auth Module will expose a `login` API to Mobicoop Platform back-office through https.

### Build the images

Finally, build the required images with :

```
make install
```

If `make` is not installed, you can simply write `sudo apt install make`

## Step 4 : Run the containers

You can now run the containers with:

```
make start
```

After a few seconds, the Kibana login form should be available at [http://localhost:5601](http://localhost:5601)

## Step 5 : Build the ElasticSearch index

```
make import
```

This command will execute a python script (`config.py`). This script reads the `config.json` file you've updated at step 2. For each Mobicoop Platform database described in this file, the script will :

- create the indices `users`, `asks` and `proposals`. The name of each index is `mobicoop_<users|asks|proposals>_<name set in config.json>` ,
- create the role `<name set in config.json>` and the Kibana user `<kibana_user set in config.json>` allowed to access the three indices created previously,
- create (if it doesn't exist) a Logstash keystore to provide the JDBC credentials to LogStash (so remove `./logstash/config/logstash.keystore.<name set in config.json>` if you change the credentials, and launch `make import` again)
- loads the data from the Mobicoop Platform Database to the ElasticSearch indices. The ElasticSearch indices are stored in the `./elasticsearchdata` directory. **Make sure that your ElasticSearch docker container has a R/W access to this directory**. To quick-fix this issue, run a `chmod a+w elasticsearchdata` (but do not do this in a production environment).

You may add a crontab entry to execute this script every day.

## Step 6 : Explore the data in Kibana

Open [http://localhost:5601](http://localhost:5601) to reach the Kibana login page.

There are two ways you can to log into Kibana :

- with the `kibana_user` and `kibana_pwd` you've set in the `config.json`. In this case, you will have a very restricted access to the Kibana features (dashboards only) and to the data (one Mobicoop Platform database)
- with the administrator account : login `elastic` and the password you've set in `.env` configuration file. In this case, you have administrator rights and will be able to create new visualizations objects, new users, and much more....

For the first time, we suggest you use the **administrator account**.

First of all, make sure that the `mobicoop_users`, `mobicoop_proposals` and `mobicoop_asks` indices have been created and filled. They should be post-fixed with the names set in the `config.json` file.

![Index management screenshot](./docs/screenshots/Index_management.png)

Then load the default Mobicoop saved objects by clicking on `Management` and `Saved objects`. Then click on `import` to import the saved objects file : They are in the `kibana/export/export.ndjson` file.

![Import screenshot](./docs/screenshots/Import_saved_objects.png)

Finally, go to `Dashboard` to display one of the default provided dashboards.
It may be necessary to reload the Kibana main page to get a correct display.

![Sample dashboard screenshot](./docs/screenshots/Dashboard_sample.png)
