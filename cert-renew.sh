#!/bin/bash
echo "SSL certificates renewal"

if [ "$EUID" -ne 0 ]
  then echo "Please run as root. You need it because you have to copy cert files."
  exit
fi

if [ -z "$1" ]
  then echo "usage : cert-renew <domain name>"
  exit
fi

# 1) get new certificates in /etc/letsencrypt/live/<your domain>
echo "Getting updated certs for domain : $1"
docker exec -it mobicoop-scope_nginx certbot renew --nginx

# 2) copy them to Kibana in /kibana/cert
echo "Copying certs to kibana/cert"
cp "/etc/letsencrypt/live/$1/fullchain.pem" ./kibana/cert
cp "/etc/letsencrypt/live/$1/privkey.pem" ./kibana/cert
chmod 640 ./kibana/cert/*
chown -R 1000:1000 ./kibana/cert



