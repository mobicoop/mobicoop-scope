#   Copyright 2020 Mobicoop
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

pink:=$(shell tput setaf 200)
blue:=$(shell tput setaf 27)
green:=$(shell tput setaf 118)
violet:=$(shell tput setaf 057)
reset:=$(shell tput sgr0) 

install:
	@docker-compose build

start:
	@docker-compose up -d --remove-orphans elasticsearch kibana
	$(info $(green)Wait for the end of the initialization process. You should display the Kibana login prompt at : http://localhost:5601/$(reset))

import:
	$(info $(green)Extracting data from Mobicoop database, and loading them to ElasticSearch $(reset))
	@python3 config.py

purge:
	./elasticsearch/mappings/purge_index.sh

debug:
	@docker-compose run logstash bash

stop:
	@docker-compose down

clean:
	@docker-compose down
	@docker rmi mobicoop-scope_logstash
	@docker rmi mobicoop-scope_elasticsearch
	@docker rmi mobicoop-scope_kibana
#	@docker rmi mobicoop-scope_nginx
#	@docker rmi mobicoop-scope_nodejs-auth
