#!/bin/bash
source .env

echo "Deleting all documents of index mobicoop_users "
curl --user elastic:$ELASTIC_PASSWORD -X POST "localhost:9200/mobicoop_users_$INSTANCE_NAME/_delete_by_query" -H 'Content-Type: application/json' -d '{ "query": { "match_all": {} }}'

echo "Deleting all documents of index mobicoop_asks "
curl --user elastic:$ELASTIC_PASSWORD -X POST "localhost:9200/mobicoop_asks_$INSTANCE_NAME/_delete_by_query" -H 'Content-Type: application/json' -d '{ "query": { "match_all": {} }}'

echo "Deleting all documents of index mobicoop_proposals "
curl --user elastic:$ELASTIC_PASSWORD -X POST "localhost:9200/mobicoop_proposals_$INSTANCE_NAME/_delete_by_query" -H 'Content-Type: application/json' -d '{ "query": { "match_all": {} }}'
