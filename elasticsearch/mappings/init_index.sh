#!/bin/bash
source .env

mapping=@"$(pwd)"/elasticsearch/mappings/users.json
echo "Building index mobicoop_users "
curl --user elastic:$ELASTIC_PASSWORD -X PUT "localhost:9200/mobicoop_users_$INSTANCE_NAME" -H 'Content-Type: application/json' -d $mapping

mapping=@"$(pwd)"/elasticsearch/mappings/proposals.json
echo "Building index mobicoop_proposals "
curl --user elastic:$ELASTIC_PASSWORD -X PUT "localhost:9200/mobicoop_proposals_$INSTANCE_NAME" -H 'Content-Type: application/json' -d $mapping

mapping=@"$(pwd)"/elasticsearch/mappings/asks.json
echo "Building index mobicoop_asks "
curl --user elastic:$ELASTIC_PASSWORD -X PUT "localhost:9200/mobicoop_asks_$INSTANCE_NAME" -H 'Content-Type: application/json' -d $mapping

