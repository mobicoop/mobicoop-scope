# Guide des indicateurs Mobicoop Scope

Ce document décrit de manière exhaustive l'ensemble des indicateurs disponibles dans Mobicoop Scope.

Version 2023-04-19

**Note:** Ce document décrit les indicateurs dans leur état actuel, y compris avec certaines incohérences ou défauts qu'il est prévu de corriger plus tard. Cette documentation est pensée pour être mise à jour à chaque changement d'un indicateur.

----

## Sommaire

[[_TOC_]]

----


----

## Dashboard utilisateurs


### Effets des filtres sur le dashboard utilisateurs


#### Filtre de date

Le filtre de date s’applique uniquement sur la date d’inscription. Par exemple, si on applique le filtre “les 3 derniers mois”, tous les indicateurs présenteront les données selon les inscrits des 3 derniers mois. 


#### Filtre géographique

Le filtre géographique permet de sélectionner un territoire parmi ceux présentés. Il inclut : 



*   les utilisateurs dont l’adresse dans leur profil correspond à l’un des territoires. 
*   les utilisateurs dont le point de départ ou d’arrivée d’une de leurs annonces (conducteur ou passager) correspond à l’un des territoires,
*   les utilisateurs dont le point de départ ou d’arrivée d’un de leurs trajets réalisés (sans annonce préalablement publiée) correspond à l’un des territoires.

### Indicateur du dashboard utilisateurs

#### Historique des inscriptions

`Diagramme représentant le nombre de nouveaux inscrits par date d’inscription.`

On peut filtrer selon les utilisateurs validés, désinscrits et non validés.

**_Exemple_**

Le 7 juillet, il y a eu 8 nouveaux inscrits sur l’interface, 6 inscrits validés et 2 non validés.


#### Nombre d’utilisateurs validés

`Nombre d’utilisateurs qui ont validé leur adresse email à l’inscription.`

Pour rappel, un utilisateur peut valider son adresse email soit en cliquant sur le lien d’activation du compte depuis le mail reçu suite à leur inscription sur la plateforme, soit en entrant le code de validation reçu par ce même mail.

**_Exemple_**

Le mois dernier, sur 174 inscriptions, il y a eu 150 utilisateurs qui ont cliqué sur le lien d'activation reçu par mail ou entré le code de validation suite à leur inscription.


#### Nombre d’utilisateurs non-validés

`Le nombre d’utilisateurs qui n’ont pas validé leur adresse email suite à leur inscription. `

Il n’est pas obligatoire de valider son adresse e-mail pour être inscrit et se connecter sur l’interface.


#### Nombre d’utilisateurs encore actifs

`Le nombre d’utilisateurs qui se sont connectés au moins une fois (sur le site web, le site mobile ou l’application mobile) au cours des 6 derniers mois.`

**_Exemple_**

Il y a 1200 utilisateurs qui se sont connectés au moins une fois, soit via le site web ou l’application au cours des 6 derniers mois.

Si on filtre sur l’année 2019, cet indicateur donnera le nombre d’utilisateurs inscrits entre le 1er janvier et le 31 décembre 2019, et qui ont été actifs dans les 6 derniers mois précédent le jour de consultation du dashboard.


#### Nombre d’utilisateurs désinscrits

`Le nombre d’utilisateurs qui ont supprimé leur compte de la plateforme.`

Attention, cet indicateur n'est comptabilisé que depuis le 28/10/2020.


**_Exemple_**

Si on filtre sur l’année 2019, on obtient le nombre d’utilisateurs inscrits en 2019 qui sont désinscrits aujourd’hui. 


#### Utilisateurs par état d’inscription

`Diagramme de répartition des utilisateurs entre ceux :`



*   `qui ont validé leur inscription ;`
*   `qui n’ont pas encore validé leur inscription;`
*   `qui se sont désinscrits.`

**_Exemple_**

94% des utilisateurs ont validé leur inscription, contre 6% qui n’ont pas cliqué sur le lien de validation.


#### Taux des recherches, annonces, sollicitations, et covoiturages acceptés des inscrits filtrés

`Pourcentage d’utilisateurs (désinscrits compris) qui ont :`



1. `fait une recherche ;`
2. `publié une annonce ;`
3. `sollicité un autre covoitureur pour un covoiturage ;`
4. `accepté une sollicitation ou eu une sollicitation acceptée.`

**_Exemple_**

Si on filtre sur l’année 2019, 80% des utilisateurs inscrits en 2019 ont réalisé une recherche sur la plateforme, 60% ont publié une annonce, 30% ont envoyé une sollicitation et 10% ont eu un trajet accepté. 


#### Nombre de désinscrits sans et avec annonce

`Le nombre de désinscrits sans annonce publiée (conducteur ou passager) et le nombre de désinscrits avec une annonce publiée.  `

L’indicateur comprend les utilisateurs validés et non validés. Attention, cet indicateur n'est comptabilisé que depuis le 28/10/2020.

**_Exemple_**

Il y a eu 23 utilisateurs qui ont supprimé leur compte alors qu’ils avaient une annonce publiée, contre 8 utilisateurs qui ont supprimé leur compte sans avoir d’annonce publiée. Si on filtre sur l’année 2019, on connaîtra le nombre d’inscrits en 2019 qui se sont désinscrits sans ou avec une annonce de publiée.


#### Nombre de désinscrits sans et avec trajet réalisé

`Le nombre de désinscrits qui n’ont pas réalisé de trajet sur la plateforme et le nombre de désinscrits qui ont réalisé au moins un covoiturage.`

Un trajet est considéré comme réalisé si le covoiturage a été validé sur l’interface et que la date du trajet est passée au moment de la recherche sur Scope.  L’indicateur comprend les utilisateurs validés et non validés. Attention, cet indicateur n'est comptabilisé que depuis le 28/10/2020.

**_Exemple_**

5 utilisateurs ont supprimé leur compte sans jamais avoir réalisé de trajet sur l’interface et 12 utilisateurs ont supprimé leur compte alors qu’ils avaient déjà réalisé au moins un trajet sur l’interface. Si on filtre sur l’année 2019, on connaîtra le nombre d’inscrits en 2019 qui se sont désinscrits, et qui ont soit un trajet réalisé soit aucun..


#### Nombre d’inscrits sans et avec annonce

`Le nombre d’inscrits sans jamais une annonce publiée (conducteur ou passager) et le nombre d’inscrits ayant eu au moins une annonce publiée`. 

L’indicateur comprend les utilisateurs validés et non validés.


#### Top 10 des communautés les plus nombreuses

`Liste des 10 communautés avec le plus d’utilisateurs (validés et non validés) qui ont rejoint la communauté. `


#### Domicile des utilisateurs

`Carte représentant les communes de résidence des utilisateurs de la plateforme.`

Attention la commune de résidence est un critère facultatif pour s’inscrire sur la plateforme, tous les utilisateurs ne l’ont pas automatiquement renseigné. Il est possible ensuite de filtrer par utilisateurs.


#### Nombre d'utilisateurs ayant déposé un avis

`Le nombre d'utilisateurs ayant déposé au moins un avis.`. 


#### Nombre d'avis déposés

`Le nombre total d'avis déposés, tous utilisateurs confondus.`. 


#### Nombre d'utilisateurs ayant fait un paiement en ligne

`Le nombre d'utilisateurs ayant fait au moins un paiement en ligne.`. 

Ne sont pris en compte que les paiements en ligne ayant été véritablement comptabilisés, et pas les tentatives de paiement ayant échoué.


#### Nombre d'utilisateurs ayant fait un paiement à la main

`Le nombre d'utilisateurs ayant fait au moins un paiement de la main à la main.`. 

Ne sont pris en compte que les paiements ayant été validés par le conducteur, pas les paiements ayant été déposés par le passager mais non validés par le conducteur.


#### Montant total des paiements en ligne

`Le montant total des paiements en ligne, tous utilisateurs confondus.`. 

Ne sont pris en compte que les paiements en ligne ayant été véritablement comptabilisés, et pas les tentatives de paiement ayant échoué.



----

## Dashboard gestionnaire de communauté


### Effets des filtres sur le dashboard gestionnaire de communauté


#### Filtre de date

Le filtre de date s’applique uniquement sur la date d’inscription de l’utilisateur, et pas sur la date à laquelle il rejoint la communauté. Par exemple, si on applique le filtre “les 3 derniers mois”, tous les indicateurs présenteront les données selon les inscrits au service des 3 derniers mois, et pas sur les nouveaux adhérents aux communautés des 3 derniers mois. 


#### Filtre géographique

Le filtre géographique permet de sélectionner un territoire parmi ceux présentés. Il inclut : 



*   les utilisateurs dont l’adresse dans leur profil correspond à l’un des territoires ; 
*   les utilisateurs dont le point de départ ou d’arrivée d’une de leurs annonces (conducteur ou passager) correspond à l’un des territoires ;
*   les utilisateurs dont le point de départ ou d’arrivée d’un de leurs trajets réalisés (sans annonce préalablement publiée) correspond à l’un des territoires.

### Indicateurs du dashboard gestionnaire de communauté

#### Adhérents à une communauté

`Nombre d’utilisateurs inscrits au service dans la période sélectionnée, et étant membres d’une communauté.`

**_Exemple_**

Sur la période de janvier à juin 2020, il y a eu 2392 nouveaux inscrits, dont 438 sont membres d’une communauté: le nombre d’adhérents à une communauté est donc de 438 sur cette période.


#### Inscriptions validées 

`Nombre d’utilisateurs qui ont validé leur adresse email à l’inscription, c'est -à -dire qui ont cliqué sur le lien d’activation du compte depuis le mail reçu ou qui ont entré le code de validation reçu par mail suite à leur inscription sur la plateform`e.

**_Exemple_**

Il y a 150 utilisateurs qui ont cliqué sur le lien d'activation reçu par mail suite à leur inscription et qui ont ainsi validé leur inscription sur la plateforme.


#### Nombre de membres par date d’inscription au service

`Graphique en bâtons représentant en abscisse les dates d’inscriptions au service, et en ordonnées le nombre d’adhérents à une communauté.`

**_Exemple_**

Le 01 octobre 2020 il y a eu 23 nouveaux inscrits quit ont rejoint depuis une communauté.


#### Offres de covoiturage

`Nombre d’annonces conducteur ou "peu importe" publiées dans une communauté, avec distinction par fréquence (ponctuel/régulier).`

On peut lire les annonces pour des trajets réguliers et ponctuels. Chaque utilisateur peut avoir plusieurs annonces publiées dans une même communauté, et publier des annonces dans des communautés différentes également.

**_Exemple_**

Au sein des communautés, il y a 25 annonces conducteur pour un trajet régulier, et 2 annonces conducteur pour un trajet ponctuel. 


#### Demandes de covoiturage

`Nombre d’annonces passager ou "peu importe" publiées dans une communauté, avec distinction par fréquence (ponctuel/régulier). `

On peut lire les annonces pour des trajets réguliers et ponctuels. Chaque utilisateur peut avoir plusieurs annonces publiées dans une même communauté, et publier des annonces dans des communautés différentes également.

**_Exemple_**

Au sein des communautés, il y a 46 annonces passager pour un trajet régulier et 4 annonces passager pour un trajet ponctuel. 


#### Nombre d’annonces par date de publication

`Par date de publication, nombre d’annonces (conducteur ou passager) attachées à une communauté.`

**_Exemple_**

Le 14 septembre 2020, il y a 5 offres et 3 demandes qui ont été publiées et qui sont associées à au moins une communauté.



----

## Dashboard annonces

### Effets des filtres sur le dashboard annonces

#### Filtre de date

Le filtre de date s'applique sur la **date de publication de l'annonce**.

### Indicateurs du dashboard annonces

#### Historique des annonces

`Graphique en histogramme du nombre d'annonces par date de publication.` L'histogramme distingue les annonces actives (pour lesquelles de nouveaux trajets sont encore possibles) des annonces inactives (annonces expirées).

#### Annonces et recherches

`Carte des annonces et recherches représentées sous forme d'un trait entre origine et destination.`

#### Nombre de recherches réalisées

`Nombre total des recherches effectuées sur la plateforme.`

Ne sont comptabilisées que les recherches faites par des utilisateurs de la plateforme, et donc excluses les recherches faites de manière externe par des SIM ou des MaaS.


#### Nombre de recherches abouties

`Nombre de recherches ayant abouti à une sollicitation.`


#### % d'annonces suivies d'une sollicitation

`Ratio d'annonces publiées qui ont eu au moins une sollicitation.`

**_Exemple_**
Sur les 145 annonces publiées dans les 30 derniers jours, 22 ont collecté au moins une première prise de contact, soit un ratio de 15,2%.



#### % d'annonces suivies d'un covoiturage accepté

`Ratio d'annonces publiées qui ont eu au moins une sollicitation acceptée.`

**_Exemple_**
Sur les 145 annonces publiées dans les 30 derniers jours, 18 ont collecté au moins une première prise de contact, soit un ratio de 12,4%.




#### Nombre total de publications d'annonces

`Nombre total de publications d'annonces`

**_Exemple_**
Dans les 30 derniers jours (durée donnée par le filtre), 145 annonces ont été publiées.


#### Nombre de prises de contact

`Nombre de sollicitations faites sur des annonces publiées dans la période filtrée.`
Attention, dans le dashboard annonces, le filtre de date ne s'applique pas sur la date de la sollicitation, mais sur la date de publication de l'annonce.

**_Exemple_**
Sur les 6905 annonces publiées en septembre-octobre (durée donnée par le filtre), 137 ont reçu au moins une prise de contact quelque soit la date de cette prise de contact.


#### Nombre de covoiturages acceptés

`Nombre d'annonces publiées qui ont eu au moins une sollicitation acceptée.`

**_Exemple_**
Sur les 6905 annonces publiées en septembre-octobre (durée donnée par le filtre), 26 ont abouti à au moins une sollicitation acceptée.




#### Nombre de publications d'annonces solidaires exclusives

`Nombre d'annonces publiées de type annonce solidaire exclusive.`

**_Exemple_**
Sur les 145 annonces publiées dans les 30 derniers jours, 2 sont de types annonce solidaire exclusive.




#### Annonces et recherches par ville de destination

`Deux tableaux des recherches et des annonces avec les 10 villes de destination les plus fréquentes.`
Pour chaque destination, est donné :

- le nombre d'occurrences (nombre de recherches, et nombre d'annonces)
- le % d'interaction (synonyme du taux de sollicitation) sur cet ensemble de recherches ou cet ensemble d'annonces
- le % de sollicitations en attente sur cet ensemble de recherches ou cet ensemble d'annonces
- le % de sollicitations acceptées sur cet ensemble de recherches ou cet ensemble d'annonces
- le % de sollicitations refusées sur cet ensemble de recherches ou cet ensemble d'annonces



#### Annonces par fréquence

`Diagramme de répartition du nombre d'annonces par fréquence : ponctuel ou régulier.`




#### Nombre de publications d'offres de covoiturage

`Nombre d'annonces conducteur ou "peu importe" publiées.`

**_Exemple_**
Sur les 145 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 50 sont des offres.



#### Nombre de publications d'annonces "peu importe"

`Nombre d'annonces "peu importe" publiées.`

**_Exemple_**
Sur les 145 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 27 sont des annonces "peu importe".



#### Nombre de publications de demandes de covoiturage

`Nombre d'annonces passager ou "peu importe" publiées.`

**_Exemple_**
Sur les 145 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 122 sont des demandes.



#### Nombres de publications d'offres par fréquence

`Nombres d'annonces conducteur ou "peu importe" publiées, selon la fréquence (ponctuel/régulier).`

**_Exemple_**
Sur les 50 offres publiées ces 30 derniers jours (durée donnée par le filtre), 32 sont des offres régulières, et 18 des offres ponctuelles.



#### Nombres de publications de demandes par fréquence

`Nombres d'annonces passager ou "peu importe" publiées, selon la fréquence (ponctuel/régulier).`

**_Exemple_**
Sur les 122 demandes publiées ces 30 derniers jours (durée donnée par le filtre), 76 sont des demandes régulières, et 46 des demandes ponctuelles.



#### Annonces en dynamique

`Nombre d'annonces conducteur, passager et total publiées en dynamique.`

**_Exemple_**
Sur les 100 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 10 sont des annonces en dynamique, dont 6 sont des offres conducteur et 4 sont des demandes passager.



#### Covoiturages dynamiques acceptés

`Nombre de mise en relation en covoiturage dynamique acceptées.`

**_Exemple_**
Sur les 100 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 4 ont abouti à une mise en relation acceptée par un conducteur en dynamique.




#### Nombres de publications d'annonces liées à une communauté

`Diagramme donnant la répartition en nombre entre les annonces liées à une communauté, et celles hors communauté.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre d'annonces que représente la portion et son pourcentage.

**_Exemple_**
Sur les 145 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 7 sont liées à une communauté et 138 sont hors communauté.




#### Nombres de publications d'offres liées à une communauté

`Diagramme donnant la répartition en nombre entre les offres liées à une communauté, et celles hors communauté.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre d'offres que représente la portion et son pourcentage.

**_Exemple_**
Sur les 50 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 2 sont liées à une communauté et 48 sont hors communauté.




#### Nombres de publications de demandes liées à une communauté

`Diagramme donnant la répartition en nombre entre les demandes liées à une communauté, et celles hors communauté.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de demandes que représente la portion et son pourcentage.

**_Exemple_**
Sur les 122 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 7 sont liées à une communauté et 115 sont hors communauté.




#### Top 10 des communautés - recherches et annonces

`Liste des 10 communautés avec le plus d’annonces et recherches liées.`

**_Exemple_**
Sur les 1327 recherches réalisées et les 122 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 51 recherches et 7 annonces sont liées à une communauté. Ce top 10 donne le classement des 10 communautés rassemblant le plus de recherches et d'annonces parmi ces 51 recherches et 7 annonces.





#### Publications d'annonces liées à un événement

`Diagramme donnant la répartition en nombre entre les annonces liées à un événement, et celles hors événement.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre d'annonces que représente la portion et son pourcentage.

**_Exemple_**
Sur les 145 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 3 sont liées à un événement et 138 sont hors événement.




#### Nombre de publications d'offres liées à un événement

`Diagramme donnant la répartition en nombre entre les offres liées à une communauté, et celles hors communauté.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre d'offres que représente la portion et son pourcentage.

**_Exemple_**
Sur les 50 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 2 sont liées à un événement et 48 sont hors événement.




#### Nombre de publications de demandes liées à un événement

**Diagramme donnant la répartition en nombre entre les demandes liées à une communauté, et celles hors communauté.**
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de demandes que représente la portion et son pourcentage.

**_Exemple_**
Sur les 122 annonces publiées ces 30 derniers jours (durée donnée par le filtre), 3 sont liées à un événement et 119 sont hors événement.




----

## Dashboard sollicitations



### Effets des filtres sur le dashboard sollicitations

#### Filtre de date

Le filtre de date s'applique sur la **date de création de la sollicitation**.



### Indicateurs du dashboard sollicitations


#### Total des sollicitations

`Nombre de sollicitations respectant les critères de filtre.`


#### États des sollicitations

`Diagramme donnant la répartition des sollicitations par état détaillé.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de sollicitation que représente la portion et son pourcentage.

**_Exemple_**
Sur les 34 sollicitations faites ces 3 derniers mois (durée donnée par le filtre), 22 ont eu un accord du conducteur (65%), 7 un accord du passager (21%), et le reste se répartit entre prise de contact et en attente.




#### Statuts des sollicitations

`Diagramme donnant la répartition des sollicitations par statut.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de sollicitation que représente la portion et son pourcentage.

**_Exemple_**
Sur les 34 sollicitations faites ces 3 derniers mois (durée donnée par le filtre), 29 ont eu un accord du conducteur (85%), et le reste se répartit entre expiré et en attente.




#### Tableau des états des sollicitations

`Tableau listant la répartition des sollicitations par état détaillé.`

**_Exemple_**
Sur les 34 sollicitations faites ces 3 derniers mois (durée donnée par le filtre), 22 ont eu un accord du conducteur, 7 un accord du passager, et le reste se répartit entre prise de contact et en attente.




#### Tableau des statuts des sollicitations

`Tableau listant la répartition des sollicitations par statut.`

**_Exemple_**
Sur les 34 sollicitations faites ces 3 derniers mois (durée donnée par le filtre), 29 ont eu un accord du conducteur, et le reste se répartit entre expiré et en attente.


#### Nombre d'utilisateurs avec une sollicitation

`Nombre d'utilisateurs ayant fait une sollicitation et respectant les filtres demandés.`

Rappel : le filtre de date s'applique sur la date de création de la sollicitation.




#### Trajets par sollicitation acceptée

**Indicateur en erreur**



#### Carte des sollicitations

`Carte des sollicitations représentées sous forme d'un trait entre origine et destination.`


#### Sollicitations liées à une communauté

`Diagramme donnant la répartition en nombre entre les sollicitations liées à une communauté, et celles hors communauté.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de sollicitations que représente la portion et son pourcentage.

**_Exemple_**
Sur les 34 sollicitations publiées ces 3 derniers mois (durée donnée par le filtre), 0 sont liées à une communauté et 34 sont hors communauté.



#### Sollicitations acceptées liées à une communauté

`Diagramme donnant la répartition en nombre entre les sollicitations acceptées liées à une communauté, et celles hors communauté.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de sollicitations acceptées que représente la portion et son pourcentage.

**_Exemple_**
Sur les 29 sollicitations acceptées publiées ces 3 derniers mois (durée donnée par le filtre), 0 sont liées à une communauté et 29 sont hors communauté.



#### Sollicitations refusées liées à une communauté

`Diagramme donnant la répartition en nombre entre les sollicitations refusées liées à une communauté, et celles hors communauté.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de sollicitations refusées que représente la portion et son pourcentage.




#### Sollicitations par communauté par fréquence

`Histogramme donnant par communauté, le nombre de sollicitations réparties par fréquence de trajet (ponctuel/régulier).`




#### Sollicitations liées à un événement

`Diagramme donnant la répartition en nombre entre les sollicitations liées à un événement, et celles hors événement.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de sollicitations que représente la portion et son pourcentage.

**_Exemple_**
Sur les 34 sollicitations publiées ces 3 derniers mois (durée donnée par le filtre), 0 sont liées à un événement et 29 sont hors événement.



#### Sollicitations acceptées liées à un événement

`Diagramme donnant la répartition en nombre entre les sollicitations acceptées liées à un événement, et celles hors événement.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de sollicitations acceptées que représente la portion et son pourcentage.

**_Exemple_**
Sur les 29 sollicitations acceptées publiées ces 3 derniers mois (durée donnée par le filtre), 0 sont liées à un événement et 34 sont hors événement.



#### Sollicitations refusées liées à un événement

`Diagramme donnant la répartition en nombre entre les sollicitations refusées liées à un événement, et celles hors événement.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de sollicitations refusées que représente la portion et son pourcentage.



#### Nombre de trajets acceptés

`Nombre de trajets acceptés associés à toutes les sollicitations acceptées et faites sur la période filtrée.`


**_Exemple_**
Les 20 sollicitations faites de septembre à octobre (durée donnée par le filtre) correspondent à 83 trajets acceptés (y compris après octobre).





#### Trajets acceptés par fréquence

`Diagramme donnant la répartition en nombre entre les trajets acceptés liées à une sollicitation ponctuelle ou régulière.`



#### Nombre de trajets acceptés

`Diagramme donnant la répartition en nombre des trajets acceptés selon l'auteur de l'acceptation: passager ou conducteur.`



#### Distance covoiturée

`Cumul des distances covoiturées des trajets acceptés associés à toutes les sollicitations acceptées et faites sur la période filtrée.`


**_Exemple_**
Les 20 sollicitations faites de septembre à octobre (durée donnée par le filtre) ont permis de planifier 83 trajets (y compris après octobre), qui totalisent 7378km covoiturés.



#### Trajets acceptés - liés à une communauté

`Diagramme donnant la répartition en nombre entre les trajets acceptés liés à une communauté, et ceux hors communauté.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de trajets que représente la portion et son pourcentage.

**_Exemple_**
Les 20 sollicitations faites de septembre à octobre (durée donnée par le filtre) ont permis de planifier 83 trajets (y compris après octobre), qui tous sont hors communauté.


#### Trajets acceptés - liés à un événement

`Diagramme donnant la répartition en nombre entre les trajets acceptés liés à un événement, et ceux hors événement.`
Au survol sur chaque portion du diagramme, s'affichent à la fois le nombre de trajets que représente la portion et son pourcentage.

**_Exemple_**
Les 20 sollicitations faites de septembre à octobre (durée donnée par le filtre) ont permis de planifier 83 trajets (y compris après octobre), qui tous sont hors événement.



#### Top 10 des km covoiturés

`Tableau des 10 couples Origine - Destination cumulant le plus de kilomètres covoiturés.`

Chaque ligne de ce tableau donne pour chaque trajet représenté par un couple commune d'Origine - commune de Destination, le cumul des kilomètres covoiturés, et le nombre de trajets que cela représente.


----

## Dashboard solidaire

### Effets des filtres sur le dashboard solidaire


#### Filtre de date

Le filtre de date s’applique uniquement sur la date d’inscription. Par exemple, si on applique le filtre “les 3 derniers mois”, tous les indicateurs présenteront les données selon les inscrits des 3 derniers mois. 


#### Filtre géographique

Le filtre géographique permet de sélectionner un territoire parmi ceux présentés. Il inclut : 



*   les utilisateurs dont l’adresse dans leur profil correspond à l’un des territoires. 
*   les utilisateurs dont le point de départ ou d’arrivée d’une de leurs annonces (conducteur ou passager) correspond à l’un des territoires,
*   les utilisateurs dont le point de départ ou d’arrivée d’un de leurs trajets réalisés (sans annonce préalablement publiée) correspond à l’un des territoires.


### Indicateurs du dashboard solidaire

#### solidaire - nb utilisateurs

`Nombre d'utilisateurs solidaires.`

**_Exemple_**
Sur les 30 derniers jours (durée donnée par le filtre), il y a eu 51 nouveaux utilisateurs solidaires.

#### solidaire - nb volontaires et bénéficiaires

`Nombre de volontaires et nombre de bénéficiaires.`

**_Exemple_**
Sur les 30 derniers jours (durée donnée par le filtre), il y a eu 42 nouveaux bénéficiaires et 9 volontaires.

#### solidaire - répartition par tranche d'âge

`Histogramme donnant la répartition des utilisateurs solidaires par tranche d'âge, en distinguant les rôles de bénéficiaire ou volontaire.`

**_Exemple_**
Sur les 51 nouveaux utilisateurs solidaires enregistrés les 30 derniers jours (durée donnée par le filtre), 27 des bénéficiaires ont moins de 30 ans et 5 des volontaires.


#### solidaire - répartition par genre

`Histogramme donnant la répartition des utilisateurs solidaires par genre, en distinguant les rôles de bénéficiaire ou volontaire.`

**_Exemple_**
Sur les 51 nouveaux utilisateurs solidaires enregistrés les 30 derniers jours (durée donnée par le filtre), 20 des bénéficiaires et 2 des volontaires sont des femmes.



#### solidaire - objet des demandes

`Tableau détaillant le nombre et le pourcentage de demandes solidaires par objet.`

**_Exemple_**
Sur les demandes solidaires enregistrées les 30 derniers jours (durée donnée par le filtre), 47 l'ont été pour "faire des achats" et 13 pour "rendez-vous médical".



----

## Dashboard événements


### Effets des filtres sur le dashboard événements


#### Filtre de date

Le filtre de date s’applique sur la date de **création de l'événement** (et pas la date de début).

### Indicateurs du dashboard événements


#### Nombre d'événements publiés

`Nombre d'événements publiés sur la période filtrée.`

**_Exemple_**
Sur l'année passée (durée donnée par le filtre), il y a eu 25 événements publiés.



#### Nombre d'événements actifs

`Nombre d'événements dont la date de début est encore dans le futur.`

**_Exemple_**
Sur l'année passée (durée donnée par le filtre), il y a 1 événement actif.



#### Nombre d'offres liées à un événement

`Nombre d'offres publiées en lien avec un événement.`

**_Exemple_**
Sur l'année passée (durée donnée par le filtre), il y a eu 9 offres liées à un événement.



#### Nombre de demandes liées à un événement

`Nombre de demandes publiées en lien avec un événement.`

**_Exemple_**
Sur l'année passée (durée donnée par le filtre), il y a eu 2 demandes liées à un événement.



#### Nombre d'événements par date de début

`Histogramme du nombre d'événements par date de début.`

La distinction est aussi faite entre événement actif et inactif.





#### Nombre de sollicitations liées à un événement

`Nombre de sollicitations liées à un événement.`

**_Exemple_**
En mai dernier (durée donnée par le filtre), sur les 3 événements créés durant cette période, il y a eu 2 sollicitations.




#### Annonces par fréquence

`Répartition des fréquences des annonces liées à un événement.`



#### Nombre de sollicitations acceptées liées à un événement

`Nombre de sollicitations acceptées liées à un événement.`

**_Exemple_**
En mai dernier (durée donnée par le filtre), sur les 3 événements créés durant cette période, il y a eu 1 sollicitation acceptée.



#### Nombre de sollicitations refusées liées à un événement

`Nombre de sollicitations refusées liées à un événement.`

**_Exemple_**
En mai dernier (durée donnée par le filtre), sur les 3 événements créés durant cette période, il y a eu 1 sollicitation refusée.



#### Nombre de trajets acceptés liés à un événement

`Nombre de trajets acceptés liés à un événement.`

**_Exemple_**
En mai dernier (durée donnée par le filtre), sur les 3 événements créés durant cette période, il y a eu 2 trajets acceptés correspondant à l'aller-retour de la sollicitation acceptée.

----

## Dashboard exploration des données

### Effets des filtres sur le dashboard exploration des données


#### Filtre de date

Le filtre de date commun à ce dashboard s’applique différement pour chaque indicateur.

### Indicateurs du dashboard exploration des données


#### Utilisateur - Tableau de suivi des inscriptions

`Nombre d'utilisateurs inscrits par mois, qu'ils soient validés ou non.`

#### proposals - tableau de suivi de publications des annonces

`Par mois, nombre d'annonces publiées, et nombre d'utilisateurs ayant publié au moins une annonce.`

#### Covoiturages - Nombre de covoiturages par mois

`Nombre de trajets de covoiturage acceptés par mois`


----

## Lexique

| Terme | Définition |
|-------|------------|
| annonce | Une annonce d’un conducteur ou d’un passager proposant un covoiturage sur la plateforme. Une annonce peut être une offre, une demande, voire les deux à la fois (cas du type "peu importe"). Une annonce est comptabilisée pour chaque sens: une annonce aller-retour publiée par un utilisateur sera ainsi comptabilisée comme deux annonces dans les indicateurs. |
| annonce (fréquence) | Les annonces peuvent avoir 2 fréquences: "ponctuel" ou "régulier". |
| annonce (type) | Une annonce peut être de 3 types: "conducteur", "passager" ou "peu importe". |
| annonce solidaire exclusive | Annonce de conducteurs qui ne veulent covoiturer que pour rendre service à des demandeurs solidaires. Leurs annonces ne sont visibles que par des opérateurs solidaires afin de les mettre en relation avec des demandes solidaires. |
| bénéficiaire | Utilisateur ayant fait une demande solidaire via la plateforme. |
| communauté | Groupe d'utilisateurs partageant un intérêt commun pour covoiturer (collègues d'une entreprise, membres d'une association). Un utilisateur peut appartenir à plusieurs communautés, et peut publier ou non une même annonce dans une ou plusieurs communautés. |
| demande | Une annonce de type "passager" ou "peu importe". Une annonce "peu importe" est donc à la fois une offre, et une demande. |
| événement | Événement (festival, concert, conférence...) auquel les utilisateurs sont invités à se rendre en covoiturage. |
| événement actif | Événement dont la date de début est dans le futur |
| interaction | Synonyme de sollicitation. Le nombre d'interactions correspond au nombre de sollicitations. |
| offre | Une annonce de type "conducteur" ou "peu importe". Une annonce "peu importe" est donc à la fois une offre, et une demande. |
| prise de contact | Comme toute sollicitation commence par une prise de contact, une "prise de contact" est par extension une sollicitation. Un indicateur "Nombre de prises de contact" est synonyme de "Nombre de sollicitations". |
| recherche | Une recherche d'un covoiturage par un visiteur ou un utilisateur connecté. Les résultats de covoiturages peuvent être obtenus soit par une recherche, soit comme la liste des covoiturages potentiels d'une annonce. |
| sollicitation | Le contact établi entre 2 covoitureurs sur la base de leurs annonces ou recherches afin de covoiturer. |
| sollicitation (état d'une) | Une sollicitation peut, en complément de son statut, avoir un des 8 états plus détaillés suivants: "prise de contact" (toute sollicitation commence par avoir cet état) / _"en attente"_ ("attente de réponse du conducteur", "attente de réponse du passager") / _"accord"_ ("accord du conducteur", "accord du passager") / _"refus"_ ("refus du conducteur", "refus du passager") / "supprimée" |
| sollicitation (statut d'une) | Une sollicitation peut avoir, en complément de son état plus détaillé, 4 statuts:  _"en attente"_ /  _"accord"_ / _"refus"_ / _"expirée"_ / _"supprimée"_ |
| trajet | Voyage aller simple effectué en covoiturage. Un aller-retour effectué en covoiturage dans chaque sens sera comptabilisé comme 2 trajets. |
| utilisateur | Personne s'ayant créé un compte sur la plateforme. |
| utilisateur solidaire | Utilisateur en lien avec les services de mobilité solidaire, qu'il soit bénéficiaire ou volontaire. |
| visiteur | Personne utilisant la plateforme sans y être connecté. |
| volontaire | Utilisateur proposant des disponibilités pour offrir des trajets de transport solidaire à des bénéficiaires. |



