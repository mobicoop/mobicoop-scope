const express = require("express");
const app = express();

var cookieParser = require("cookie-parser");
app.use(cookieParser());

//app.use(express.json())

var cors = require("cors");
var corsOptions = {
  origin: true,
  credentials: true,
  optionsSuccessStatus: 200,
};
app.use(cors(corsOptions));

// To build a new token
uuid = require("uuid");

// To query Mobicoop API
const fetch = require("node-fetch");

// Read json configuration file
const fs = require("fs");
let config = {};
try {
  let jsondata = fs.readFileSync("../config.json");
  config = JSON.parse(jsondata);
} catch (error) {
  console.error(error);
}

const findConfigByInstanceName = (name) =>
  config && config.find((e) => e.name === name);

// A new token is added to this object after each successfull connection.
var listOfAuthorizedToken = {};

const convertToBase64 = (s) => Buffer.from(s).toString("base64");

// Login route with CORS
app.get("/login/:instance", async (req, responseToLogin) => {
  // This endpoint add a new token to the white list.
  // The Authorization header should have a valid Mobicoop Token.
  // With this token, we query the Mobicoop API to check the user's rights
  const requestAuthorisationheader = req.get("Authorization");

  // What instance does the user want to get data from ?
  const instance = req.params.instance;

  // What is the Mobicoop API to use in order to check the permission ?
  const instanceConfiguration = findConfigByInstanceName(instance);

  console.log("instance:", instance);
  console.log("instanceConfiguration:", instanceConfiguration);
  console.log("requestAuthorisationheader:", requestAuthorisationheader);

  // Do not go further if something is missing
  if (!instanceConfiguration || !requestAuthorisationheader) {
    responseToLogin.statusCode = 401;
    responseToLogin.send("Access denied.");
    return;
  }

  // Fetching Mobicoop API
  const apiPermissions = instanceConfiguration.mobicoop_api + "/permissions";
  console.log("Fetching :", apiPermissions);
  await fetch(apiPermissions, {
    method: "get",
    headers: {
      "Content-Type": "application/json",
      Authorization: requestAuthorisationheader,
    },
  })
    .then((res) => res.json())
    .then((permissionsResponse) => {
      console.log("permissionsResponse  :", permissionsResponse);

      const permissions = permissionsResponse["hydra:member"] || [];
      const allowed = permissions.includes("access_admin");

      if (allowed) {
        // Build a new token
        const newToken = uuid.v4();

        // Add the token to the list
        listOfAuthorizedToken[newToken] = convertToBase64(
          instanceConfiguration.kibana_user +
            ":" +
            instanceConfiguration.kibana_pwd
        );
        responseToLogin.cookie("mobicoop_kibana_token", newToken, {
          sameSite: "none", // http://expressjs.com/en/4x/api.html#res.cookie
          secure: true,
        });
        responseToLogin.statusCode = 200;
        return responseToLogin.send(
          "Authorized. Check the kibana_token cookie."
        );
      } else {
        responseToLogin.statusCode = 401;
        return responseToLogin.send("Access denied. ");
      }
    })
    .catch((error) => {
      console.log("Fetching error :", error);
      responseToLogin.statusCode = 401;
      return responseToLogin.send("Access denied. " + JSON.stringify(error));
    });
});

// Check cookie
app.get("/check", (req, res) => {
  console.log("Checking cookie : ", req.cookies);
  res.statusCode = 200;
  const token = req.cookies && req.cookies.mobicoop_kibana_token;
  if (token) {
    // Check token validity
    if (listOfAuthorizedToken.hasOwnProperty(token)) {
      return res.send("Authorized");
    }
  }
  res.send("Access denied.");
});

// Clear cookie (logout)
app.get("/logout", (req, res) => {
  res.clearCookie("mobicoop_kibana_token");
  res.statusCode = 200;
  return res.send("Logged out");
});

// API entrypoint for Nginx auth_request
app.get("/", (req, res) => {
  res.statusCode = 401;
  const requestedUri = req.headers["x-original-uri"]; // for log purpose only
  const token = req.cookies && req.cookies.mobicoop_kibana_token;

  if (token) {
    // Check token validity
    if (listOfAuthorizedToken.hasOwnProperty(token)) {
      const header = listOfAuthorizedToken[token];
      res.header("Authorization", "Basic " + header);
      res.statusCode = 200;
      console.log("[Authorized] ", requestedUri);
      return res.send("Authorized");
    } else {
      console.log("[Access denied] ", requestedUri);
      res.send("Access denied.");
    }
  } else {
    console.log("[No mobicoop_kibana_token] ", requestedUri);
    res.send("No mobicoop_kibana_token. Access denied.");
  }
});

app.listen(process.env.APP_PORT || 5602, () =>
  console.log(
    `Authentication server listening on port ${process.env.APP_PORT || 5602}!`
  )
);
